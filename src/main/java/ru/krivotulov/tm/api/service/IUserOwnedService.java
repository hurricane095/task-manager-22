package ru.krivotulov.tm.api.service;

import ru.krivotulov.tm.api.repository.IUserOwnedRepository;
import ru.krivotulov.tm.enumerated.Sort;
import ru.krivotulov.tm.model.AbstractUserOwnedModel;

import java.util.List;

/**
 * IUserOwnedService
 *
 * @author Aleksey_Krivotulov
 */
public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M>, IService<M> {

    List<M> findAll(String userId, Sort sort);

}
