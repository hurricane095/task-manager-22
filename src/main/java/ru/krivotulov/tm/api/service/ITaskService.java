package ru.krivotulov.tm.api.service;

import ru.krivotulov.tm.enumerated.Status;
import ru.krivotulov.tm.model.Task;

import java.util.Date;
import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {

    Task create(String userId, String name);

    Task create(String userId, String name, String description);

    Task create(String userId, String name, String description, Date dateBegin, Date dateEnd);

    List<Task> findAllByProjectId(String userId, String projectId);

    Task updateById(String userId, String id, String name, String description);

    Task updateByIndex(String userId, Integer index, String name, String description);

    Task changeStatusById(String userId, String id, Status status);

    Task changeStatusByIndex(String userId, Integer index, Status status);

}
