package ru.krivotulov.tm.api.service;

public interface IProjectTaskService {

    void bindTaskToProject(String userId, String projectId, String taskId);

    void unbindTaskFromProject(String userId, String taskId, String projectId);

    void removeProjectById(String userId, String projectId);

}
