package ru.krivotulov.tm.api.model;

import ru.krivotulov.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
