package ru.krivotulov.tm.api.repository;

import ru.krivotulov.tm.model.AbstractModel;
import ru.krivotulov.tm.model.Project;
import ru.krivotulov.tm.model.Task;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

/**
 * IRepository
 *
 * @author Aleksey_Krivotulov
 */
public interface IRepository<M extends AbstractModel> {

    void clear();

    M add(M model);

    List<M> findAll();

    List<M> findAll(Comparator<M> comparator);

    M findOneById(String id);

    M findOneByIndex(Integer index);

    M delete(M model);

    M deleteById(String id);

    M deleteByIndex(Integer index);

    boolean existsById(String id);

    int getSize();

    void removeAll(Collection<M> collection);

}
