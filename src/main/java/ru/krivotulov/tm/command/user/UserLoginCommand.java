package ru.krivotulov.tm.command.user;

import ru.krivotulov.tm.enumerated.Role;
import ru.krivotulov.tm.util.TerminalUtil;

/**
 * UserLoginCommand
 *
 * @author Aleksey_Krivotulov
 */
public final class UserLoginCommand extends AbstractUserCommand {

    public static final String NAME = "user-login";

    public static final String DESCRIPTION = "Log in.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("ENTER LOGIN: ");
        String login = TerminalUtil.readLine();
        System.out.println("ENTER PASSWORD: ");
        String password = TerminalUtil.readLine();
        getAuthService().login(login, password);
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}
