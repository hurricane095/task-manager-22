package ru.krivotulov.tm.command.user;

import ru.krivotulov.tm.enumerated.Role;
import ru.krivotulov.tm.util.TerminalUtil;

/**
 * UserockCommand
 *
 * @author Aleksey_Krivotulov
 */
public final class UserLockCommand extends AbstractUserCommand{

    public static final String NAME = "user-lock";

    public static final String DESCRIPTION = "User lock.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public Role[] getRoles() {
        return new Role[] {Role.ADMINISTRATOR};
    }

    @Override
    public void execute() {
        System.out.println("[USER LOCK]");
        System.out.println("ENTER LOGIN: ");
        final String login = TerminalUtil.readLine();
        getUserService().lockUserByLogin(login);
    }

}
