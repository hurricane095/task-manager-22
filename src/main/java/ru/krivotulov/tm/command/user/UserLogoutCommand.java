package ru.krivotulov.tm.command.user;

import ru.krivotulov.tm.enumerated.Role;

/**
 * UserLogoutCommand
 *
 * @author Aleksey_Krivotulov
 */
public final class UserLogoutCommand extends AbstractUserCommand {

    public static final String NAME = "user-logout";

    public static final String DESCRIPTION = "Log out.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        getAuthService().logout();
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
