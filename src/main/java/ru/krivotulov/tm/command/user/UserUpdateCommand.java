package ru.krivotulov.tm.command.user;

import ru.krivotulov.tm.enumerated.Role;
import ru.krivotulov.tm.util.TerminalUtil;

/**
 * UserUpdateCommand
 *
 * @author Aleksey_Krivotulov
 */
public final class UserUpdateCommand extends AbstractUserCommand {

    public static final String NAME = "user-update";

    public static final String DESCRIPTION = "User update.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        String userId = getAuthService().getUserId();
        System.out.println("ENTER FIRST NAME: ");
        String firstName = TerminalUtil.readLine();
        System.out.println("ENTER LAST NAME: ");
        String lastName = TerminalUtil.readLine();
        System.out.println("ENTER MIDDLE NAME: ");
        String middleName = TerminalUtil.readLine();
        getUserService().updateUser(userId, firstName, lastName, middleName);
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
