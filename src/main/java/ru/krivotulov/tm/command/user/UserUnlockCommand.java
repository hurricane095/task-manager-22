package ru.krivotulov.tm.command.user;

import ru.krivotulov.tm.enumerated.Role;
import ru.krivotulov.tm.util.TerminalUtil;

/**
 * UserUnLockCommand
 *
 * @author Aleksey_Krivotulov
 */
public final class UserUnlockCommand extends AbstractUserCommand{

    public static final String NAME = "user-unlock";

    public static final String DESCRIPTION = "User unlock.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public Role[] getRoles() {
        return new Role[] {Role.ADMINISTRATOR};
    }

    @Override
    public void execute() {
        System.out.println("[USER UNLOCK]");
        System.out.println("ENTER LOGIN: ");
        final String login = TerminalUtil.readLine();
        getUserService().unlockUserByLogin(login);
    }

}
