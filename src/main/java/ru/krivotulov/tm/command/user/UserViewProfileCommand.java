package ru.krivotulov.tm.command.user;

import ru.krivotulov.tm.enumerated.Role;
import ru.krivotulov.tm.model.User;

/**
 * UserViewProfileCommand
 *
 * @author Aleksey_Krivotulov
 */
public final class UserViewProfileCommand extends AbstractUserCommand {

    public static final String NAME = "user-show-profile";

    public static final String DESCRIPTION = "User show profile.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final User user = getAuthService().getUser();
        showUser(user);
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
