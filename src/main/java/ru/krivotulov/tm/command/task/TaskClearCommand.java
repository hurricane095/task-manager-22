package ru.krivotulov.tm.command.task;

/**
 * TaskClearCommand
 *
 * @author Aleksey_Krivotulov
 */
public class TaskClearCommand extends AbstractTaskCommand {

    public static final String NAME = "task-clear";

    public static final String DESCRIPTION = "Remove all tasks.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[TASK CLEAR]");
        final String userId = getUserId();
        getTaskService().clear(userId);
    }

}
