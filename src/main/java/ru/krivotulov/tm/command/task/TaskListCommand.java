package ru.krivotulov.tm.command.task;

import ru.krivotulov.tm.enumerated.Sort;
import ru.krivotulov.tm.model.Task;
import ru.krivotulov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

/**
 * TaskListCommand
 *
 * @author Aleksey_Krivotulov
 */
public class TaskListCommand extends AbstractTaskCommand {

    public static final String NAME = "task-list";

    public static final String DESCRIPTION = "Display task list.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT: ");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.readLine();
        final Sort sort = Sort.toSort(sortType);
        final String userId = getUserId();
        final List<Task> taskList = getTaskService().findAll(userId, sort);
        renderTasks(taskList);
    }

}
