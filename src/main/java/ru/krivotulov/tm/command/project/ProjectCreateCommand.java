package ru.krivotulov.tm.command.project;

import ru.krivotulov.tm.util.TerminalUtil;

import java.util.Date;

/**
 * ProjectCreateCommand
 *
 * @author Aleksey_Krivotulov
 */
public class ProjectCreateCommand extends AbstractProjectCommand {

    public static final String NAME = "project-create";

    public static final String DESCRIPTION = "Create new project.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME: ");
        final String name = TerminalUtil.readLine();
        System.out.println("ENTER DESCRIPTION: ");
        final String description = TerminalUtil.readLine();
        System.out.println("ENTER DATE BEGIN: ");
        final Date dateBegin = TerminalUtil.nextDate();
        System.out.println("ENTER DATE END: ");
        final Date dateEnd = TerminalUtil.nextDate();
        final String userId = getUserId();
        getProjectService().create(userId, name, description, dateBegin, dateEnd);
    }

}
