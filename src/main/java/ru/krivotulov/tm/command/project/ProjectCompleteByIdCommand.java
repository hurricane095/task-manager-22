package ru.krivotulov.tm.command.project;

import ru.krivotulov.tm.enumerated.Status;
import ru.krivotulov.tm.util.TerminalUtil;

/**
 * ProjectCompleteByIdCommand
 *
 * @author Aleksey_Krivotulov
 */
public class ProjectCompleteByIdCommand extends AbstractProjectCommand {

    public static final String NAME = "project-complete-by-id";

    public static final String DESCRIPTION = "Complete project by id.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[START TASK BY ID]");
        System.out.println("ENTER ID: ");
        final String id = TerminalUtil.readLine();
        final String userId = getUserId();
        getProjectService().changeStatusById(userId, id, Status.COMPLETED);
    }

}
