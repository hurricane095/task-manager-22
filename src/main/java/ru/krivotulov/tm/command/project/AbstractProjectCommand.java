package ru.krivotulov.tm.command.project;

import ru.krivotulov.tm.api.service.IAuthService;
import ru.krivotulov.tm.api.service.IProjectService;
import ru.krivotulov.tm.api.service.IProjectTaskService;
import ru.krivotulov.tm.command.AbstractCommand;
import ru.krivotulov.tm.enumerated.Role;
import ru.krivotulov.tm.enumerated.Status;
import ru.krivotulov.tm.model.Project;
import ru.krivotulov.tm.util.DateUtil;

/**
 * AbstractProjectCommand
 *
 * @author Aleksey_Krivotulov
 */
public abstract class AbstractProjectCommand extends AbstractCommand {

    @Override
    public String getArgument() {
        return null;
    }

    protected IProjectService getProjectService() {
        return serviceLocator.getProjectService();
    }

    protected IProjectTaskService getProjectTaskService() {
        return serviceLocator.getProjectTaskService();
    }

    public IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    public String getUserId(){
        return getAuthService().getUserId();
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    protected void showProject(Project project) {
        if (project == null) return;
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("ID: " + project.getId());
        System.out.println("STATUS: " + Status.toName(project.getStatus()));
        System.out.println("CREATED: " + DateUtil.toString(project.getCreated()));
        System.out.println("DATE BEGIN: " + DateUtil.toString(project.getDateBegin()));
        System.out.println("DATE END: " + DateUtil.toString(project.getDateEnd()));
        final Status status = project.getStatus();
        if (status != null) System.out.println("STATUS: " + status.getDisplayName());
    }

}
