package ru.krivotulov.tm.command.system;

import ru.krivotulov.tm.api.model.ICommand;
import ru.krivotulov.tm.command.AbstractCommand;

import java.util.Collection;

/**
 * ArgumentsCommand
 *
 * @author Aleksey_Krivotulov
 */
public class ArgumentsCommand extends AbstractSystemCommand {

    public static final String NAME = "arguments";

    public static final String DESCRIPTION = "Display argument list.";

    public static final String ARGUMENT = "-arg";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final ICommand command : commands) {
            final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

}
