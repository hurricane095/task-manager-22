package ru.krivotulov.tm.component;

import ru.krivotulov.tm.api.repository.ICommandRepository;
import ru.krivotulov.tm.api.repository.IProjectRepository;
import ru.krivotulov.tm.api.repository.ITaskRepository;
import ru.krivotulov.tm.api.repository.IUserRepository;
import ru.krivotulov.tm.api.service.*;
import ru.krivotulov.tm.command.AbstractCommand;
import ru.krivotulov.tm.command.project.*;
import ru.krivotulov.tm.command.projecttask.BindTaskToProjectCommand;
import ru.krivotulov.tm.command.projecttask.UnbindTaskFromProjectCommand;
import ru.krivotulov.tm.command.system.*;
import ru.krivotulov.tm.command.task.*;
import ru.krivotulov.tm.command.user.*;
import ru.krivotulov.tm.enumerated.Role;
import ru.krivotulov.tm.exception.system.ArgumentNotSupportedException;
import ru.krivotulov.tm.exception.system.CommandNotSupportedException;
import ru.krivotulov.tm.repository.CommandRepository;
import ru.krivotulov.tm.repository.ProjectRepository;
import ru.krivotulov.tm.repository.TaskRepository;
import ru.krivotulov.tm.repository.UserRepository;
import ru.krivotulov.tm.service.*;
import ru.krivotulov.tm.util.TerminalUtil;

public final class Bootstrap implements IServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ILoggerService loggerService = new LoggerService();

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository, taskRepository, projectRepository);

    private final IAuthService authService = new AuthService(userService);

    {
        registry(new AboutCommand());
        registry(new ArgumentsCommand());
        registry(new CommandsCommand());
        registry(new ExitCommand());
        registry(new HelpCommand());
        registry(new InfoCommand());
        registry(new VersionCommand());
        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectListCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());
        registry(new BindTaskToProjectCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskClearCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskCreateCommand());
        registry(new TaskListCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowListByProjectCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new UnbindTaskFromProjectCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
        registry(new UserChangePasswordCommand());
        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserRegistryCommand());
        registry(new UserUpdateCommand());
        registry(new UserViewProfileCommand());
        registry(new UserRemoveCommand());
        registry(new UserLockCommand());
        registry(new UserUnlockCommand());
    }

    private void registry(final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void initUsers() {
        userService.create("user", "user", "usre@mail.ru");
        userService.create("admin", "admin", Role.ADMINISTRATOR);
    }

    private void init() {
        final String userId = userService.findByLogin("user").getId();
        projectService.create(userId, "proj", "proj");
        taskService.create(userId, "task", "task");
    }

    public void run(final String[] args) {
        if (runArgument(args)) System.exit(0);
        initUsers();
        init();
        initLogger();
        process();
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        subscribeSystemExit();
    }

    private void subscribeSystemExit() {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
            }
        });
    }

    private void process() {
        while (true) {
            try {
                System.out.println("ENTER COMMAND:");
                final String command = TerminalUtil.readLine();
                runCommand(command);
                loggerService.command(command);
                System.out.println("[OK]");
            } catch (final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    private void runCommand(final String command) {
        final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private boolean runArgument(final String[] args) {
        if (args == null || args.length < 1) return false;
        final String param = args[0];
        runArgument(param);
        return true;
    }

    private void runArgument(final String argument) {
        final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public ILoggerService getLoggerService() {
        return loggerService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

}
