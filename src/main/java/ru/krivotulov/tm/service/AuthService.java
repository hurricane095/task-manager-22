package ru.krivotulov.tm.service;

import ru.krivotulov.tm.api.service.IAuthService;
import ru.krivotulov.tm.api.service.IUserService;
import ru.krivotulov.tm.enumerated.Role;
import ru.krivotulov.tm.exception.field.LoginEmptyException;
import ru.krivotulov.tm.exception.field.PasswordEmptyException;
import ru.krivotulov.tm.exception.system.AccessDeniedException;
import ru.krivotulov.tm.exception.system.IncorrectLoginOrPasswordException;
import ru.krivotulov.tm.exception.system.PermissionException;
import ru.krivotulov.tm.model.User;
import ru.krivotulov.tm.util.HashUtil;

import java.util.Arrays;

/**
 * AuthService
 *
 * @author Aleksey_Krivotulov
 */
public class AuthService implements IAuthService {

    private final IUserService userService;

    private String userId;

    public AuthService(IUserService userService) {
        this.userService = userService;
    }

    @Override
    public void login(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new IncorrectLoginOrPasswordException();
        if(user.isLocked()) throw new AccessDeniedException();
        if (!user.getPasswordHash().equals(HashUtil.salt(password))) {
            throw new IncorrectLoginOrPasswordException();
        }
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public User registry(final String login,
                         final String password,
                         final String email) {
        return userService.create(login, password, email);
    }

    @Override
    public User getUser() {
        final String userId = getUserId();
        return userService.findOneById(userId);
    }

    @Override
    public String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public boolean isAuth() {
        return userId == null;
    }

    @Override
    public void checkRoles(final Role[] roles) {
        if (roles == null) return;
        final User user = getUser();
        final Role role = user.getRole();
        if (role == null) throw new PermissionException();
        final boolean hasRole = Arrays.asList(roles).contains(role);
        if (!hasRole) throw new PermissionException();
    }

}
