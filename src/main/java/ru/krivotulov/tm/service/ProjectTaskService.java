package ru.krivotulov.tm.service;

import ru.krivotulov.tm.api.repository.IProjectRepository;
import ru.krivotulov.tm.api.repository.ITaskRepository;
import ru.krivotulov.tm.api.service.IProjectTaskService;
import ru.krivotulov.tm.exception.entity.ProjectNotFoundException;
import ru.krivotulov.tm.exception.entity.TaskNotFoundException;
import ru.krivotulov.tm.exception.field.UserIdEmptyException;
import ru.krivotulov.tm.model.Task;
import ru.krivotulov.tm.util.ListUtil;

import java.util.List;
import java.util.Optional;

public class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectTaskService(final IProjectRepository projectRepository,
                              final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public void bindTaskToProject(final String userId, String projectId, String taskId) {
        if(userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if(projectId == null || projectId.isEmpty()) return;
        if(taskId == null || taskId.isEmpty()) return;
        if(!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        final Task task = taskRepository.findOneById(userId, taskId);
        if(task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
    }

    @Override
    public void unbindTaskFromProject(final String userId, String taskId, String projectId) {
        if(userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if(projectId == null || projectId.isEmpty()) return;
        if(taskId == null || taskId.isEmpty()) return;
        if(!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        final Task task = taskRepository.findOneById(userId, taskId);
        if(task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
    }

    @Override
    public void removeProjectById(final String userId, String projectId) {
        if(userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if(projectId == null || projectId.isEmpty()) return;
        final List<Task> taskList = taskRepository.findAllByProjectId(userId, projectId);
        Optional.ofNullable(ListUtil.emptyListToNull(taskList))
                .ifPresent(list -> list.forEach(task -> taskRepository.deleteById(userId, task.getId())));
        projectRepository.deleteById(userId, projectId);
    }

}
