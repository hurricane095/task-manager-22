package ru.krivotulov.tm.service;

import ru.krivotulov.tm.api.repository.IProjectRepository;
import ru.krivotulov.tm.api.repository.ITaskRepository;
import ru.krivotulov.tm.api.repository.IUserRepository;
import ru.krivotulov.tm.api.service.IUserService;
import ru.krivotulov.tm.enumerated.Role;
import ru.krivotulov.tm.exception.field.*;
import ru.krivotulov.tm.model.User;
import ru.krivotulov.tm.util.HashUtil;

import java.util.Optional;

/**
 * UserService
 *
 * @author Aleksey_Krivotulov
 */
public class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    private final ITaskRepository taskRepository;

    private final IProjectRepository projectRepository;

    public UserService(final IUserRepository userRepository,
                       final ITaskRepository taskRepository,
                       final IProjectRepository projectRepository
    ) {
        super(userRepository);
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public User findByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return repository.findByLogin(login);
    }

    @Override
    public Boolean isLoginExist(final String login) {
        if (login == null || login.isEmpty()) return false;
        return repository.isLoginExist(login);
    }

    @Override
    public Boolean isEmailExist(final String email) {
        if (email == null || email.isEmpty()) return false;
        return repository.isEmailExist(email);
    }

    @Override
    public User findByEmail(final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return repository.findByEmail(email);
    }


    @Override
    public User create(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isLoginExist(login)) throw new LoginExistsException();
        return repository.create(login, password);
    }

    @Override
    public User create(final String login, final String password, final String email) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (isLoginExist(login)) throw new LoginExistsException();
        if (isEmailExist(email)) throw new EmailExistsException();
        return repository.create(login, password, email);
    }

    @Override
    public User create(final String login, final String password, final Role role) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isLoginExist(login)) throw new LoginExistsException();
        return repository.create(login, password, role);
    }

    @Override
    public User delete(final User model) {
        if(model == null) return null;
        final User user = super.delete(model);
        if(user == null) return null;
        final String userId = user.getId();
        taskRepository.clear(userId);
        projectRepository.clear(userId);
        return user;
    }

    @Override
    public User deleteByLogin(final String login){
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final User user = findByLogin(login);
        return delete(user);
    }

    @Override
    public User deleteByEmail(final String email){
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        final User user = findByEmail(email);
        return delete(user);
    }

    @Override
    public User setPassword(final String userId, final String password) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        return Optional.ofNullable(findOneById(userId))
                .map(user -> {
                    final String hash = HashUtil.salt(password);
                    user.setPasswordHash(hash);
                    return user;
                }).orElse(null);
    }

    @Override
    public User updateUser(final String userId,
                           final String firstName,
                           final String lastName,
                           final String middleName) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        return Optional.ofNullable(findOneById(userId))
                .map(user -> {
                    user.setFirstName(firstName);
                    user.setLastName(lastName);
                    user.setMiddleName(middleName);
                    return user;
                }).orElse(null);
    }

    @Override
    public void lockUserByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final User user = findByLogin(login);
        user.setLocked(true);
    }

    @Override
    public void unlockUserByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final User user = findByLogin(login);
        user.setLocked(false);
    }
}
